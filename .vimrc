execute pathogen#infect()
let g:NERDTreeDirArrows=0
au FileType go au BufWritePre <buffer> %! goimports
set rtp+=/usr/bin/go/misc/vim 
vmap '' :w !pbcopy<CR><CR>
set ai
set ts=2
set sw=2
set expandtab
set number
set nowrap
set guioptions+=b

noremap <F1>          :w<CR>
vnoremap <F1>         <C-C>:w<CR>
inoremap <F1>         <C-O>:w<CR>

map <F2> :NERDTree<CR>
map <Home> <S-^>
imap <Home> <Esc><S-^>i
map <S-Down> v
map <S-Up> v
map <S-Left> v
map <S-Right> v
imap <S-Down> <C-o>v
imap <S-Up> <C-o>v
imap <S-Left> <C-o>v
imap <S-Right> <C-o>v
map <C-Up> <PageUp>
map <C-Down> <PageDown>
map <C-Left> <Home>
map <C-Right> <End>
imap <C-Up> <PageUp>
imap <C-Down> <PageDown>
imap <C-Left> <Home>
imap <C-Right> <End>
map <C-v> +gP
imap <C-v> <Esc>+gPi
map <F6> :call MoveToNextTab()<CR><C-w>L
map <F5> :call MoveToPrevTab()<CR><C-w>L
source ~/.vim/plugin/comments.vim
set nocompatible
syntax on
set smartindent
set autoindent
set nobackup
colorscheme Tomorrow-Night
"if has("gui_running") 
"color codeschool
"endif 
set guifont=Monaco:h12
set ttyfast
set mouse=a
set ttymouse=xterm2
set backspace=indent,eol,start
set clipboard=unnamed
hi TabLineFill ctermfg=LightGreen ctermbg=Darkgray
hi TabLine ctermfg=Gray ctermbg=Black
hi TabLineSel ctermfg=White ctermbg=LightBlue
hi Title ctermfg=LightBlue ctermbg=White
set laststatus=2
set statusline=
set statusline+=%7*\[%n]                                  "buffernr
set statusline+=%1*\ %<%F\                                "File+path
set statusline+=%2*\ %y\                                  "FileType
"set statusline+=%3*\ %{''.(&fenc!=''?&fenc:&enc).''}      "Encoding
"set statusline+=%3*\ %{(&bomb?\",BOM\":\"\")}\            "Encoding2
"set statusline+=%4*\ %{&ff}\                              "FileFormat (dos/unix..) 
"set statusline+=%5*\ %{&spelllang}\%{HighlightSearch()}\  "Spellanguage & Highlight on?
set statusline+=%8*\ %=\ row:%l/%L\ (%03p%%)\             "Rownumber/total (%)
set statusline+=%9*\ col:%03c\                            "Colnr
set statusline+=%0*\ \ %m%r%w\ %P\ \                      "Modified? Readonly? Top/bot.
au BufRead,BufNewFile *.ds set filetype=javascript
au BufRead,BufNewFile *.isml set filetype=html
autocmd InsertEnter,InsertLeave * set cul!

au VimEnter * NERDTreeToggle

" Highlight searches
set hlsearch
" Ignore case of searches
set ignorecase
" Highlight dynamically as pattern is typed
set incsearch
" Centralize backups, swapfiles and undo history
set backupdir=~/.vim/backups
set directory=~/.vim/swaps
if exists("&undodir")
        set undodir=~/.vim/undo
endif
function! HighlightSearch()
  if &hls
    return 'H'
  else
    return ''
  endif
endfunction

hi User1 ctermfg=White	ctermbg=DarkBlue
hi User2 ctermfg=White  ctermbg=Black
hi User3 ctermfg=White  ctermbg=Black
hi User4 ctermfg=White  ctermbg=Black
hi User5 ctermfg=White  ctermbg=Black
hi User7 ctermfg=White  ctermbg=DarkBlue
hi User8 ctermfg=White  ctermbg=Black
hi User9 ctermfg=White  ctermbg=Black
hi User0 ctermfg=White  ctermbg=Black
hi User1 guifg=White	guibg=DarkBlue
hi User2 guifg=White  guibg=Black
hi User3 guifg=White  guibg=Black
hi User4 guifg=White  guibg=Black
hi User5 guifg=White  guibg=Black
hi User7 guifg=White  guibg=DarkBlue
hi User8 guifg=White  guibg=Black
hi User9 guifg=White  guibg=Black
hi User0 guifg=White  guibg=Black

function MoveToPrevTab()
  "there is only one window
  if tabpagenr('$') == 1 && winnr('$') == 1
    return
  endif
  "preparing new window
  let l:tab_nr = tabpagenr('$')
  let l:cur_buf = bufnr('%')
  if tabpagenr() != 1
    close!
    if l:tab_nr == tabpagenr('$')
      tabprev
    endif
    sp
  else
    close!
    exe "0tabnew"
  endif
  "opening current buffer in new window
  exe "b".l:cur_buf
endfunc

function MoveToNextTab()
  "there is only one window
  if tabpagenr('$') == 1 && winnr('$') == 1
    return
  endif
  "preparing new window
  let l:tab_nr = tabpagenr('$')
  let l:cur_buf = bufnr('%')
  if tabpagenr() < tab_nr
    close!
    if l:tab_nr == tabpagenr('$')
      tabnext
    endif
    sp
  else
    close!
    tabnew
  endif
  "opening current buffer in new window
  exe "b".l:cur_buf
endfunc


" Rename tabs to show tab number.
" (Based on http://stackoverflow.com/questions/5927952/whats-implementation-of-vims-default-tabline-function)
if exists("+showtabline")
    function! MyTabLine()
        let s = ''
        let wn = ''
        let t = tabpagenr()
        let i = 1
        while i <= tabpagenr('$')
            let buflist = tabpagebuflist(i)
            let winnr = tabpagewinnr(i)
            let s .= '%' . i . 'T'
            let s .= (i == t ? '%1*' : '%2*')
            let s .= ' '
            let wn = tabpagewinnr(i,'$')

            let s .= '%#TabNum#'
            let s .= i
            " let s .= '%*'
            let s .= (i == t ? '%#TabLineSel#' : '%#TabLine#')
            let bufnr = buflist[winnr - 1]
            let file = bufname(bufnr)
            let buftype = getbufvar(bufnr, 'buftype')
            if buftype == 'nofile'
                if file =~ '\/.'
                    let file = substitute(file, '.*\/\ze.', '', '')
                endif
            else
                let file = fnamemodify(file, ':p:t')
            endif
            if file == ''
                let file = '[No Name]'
            endif
            let s .= ' ' . file . ' '
            let i = i + 1
        endwhile
        let s .= '%T%#TabLineFill#%='
        let s .= (tabpagenr('$') > 1 ? '%999XX' : 'X')
        return s
    endfunction
    set stal=2
    set tabline=%!MyTabLine()
    set showtabline=1
    highlight link TabNum Special
endif
